#!/bin/bash
#
# INSTALLATION
#   ~/developer-tools $ sudo ln -sf ${PWD}/script/convert_file_coding.sh /usr/local/bin/convert_file_coding
#
# USAGE
# Just call the script in the root directory of the project
#   ~/developer-tools $ convert_file_coding
#

## Argument verification
if [ $# -ne 0 ]; then
	echo "Usage : $0
	NOTE: Just call the script in the root directory of the project"
	exit 1
fi

## Verify iconv is installed
if [ "." == ".$(which iconv)" ]; then
	echo "Please install first 'iconv' package"
	exit 1
fi

INVALID_FORMATS=('WINDOWS-1252' 'ISO-8859')
VALID_FORMATS=('ASCII' 'UTF-8')
TARGET_FORMAT=${VALID_FORMATS[1]}


for i in `find . -name '*.h' -o -name '*.cpp' -o -name '*.hpp' -o -name '*.c'`; do
	line=$(file $i)
	IFS=':' read -a filepath <<< "$line"
	file=${filepath[0]}
	targetfile=${filepath}.new
	filestat=${filepath[1]}

	fromcoding="."
	for srcformat in "${INVALID_FORMATS[@]}"; do
		if [[ $filestat == *${srcformat}* ]]; then
			fromcoding=$srcformat
			break
		fi
	done

	if [ $fromcoding == "." ]; then
		for srcformat in "${VALID_FORMATS[@]}"; do
			if [[ $filestat == *${srcformat}* ]]; then
				fileokay=true
				break
			fi
		done

		if [ ! $fileokay ]; then
			echo "File $file doesn't have a valid format $fromcoding";
		fi
		continue
	fi

	echo -n "Converting $file .. "

	if [ $fromcoding == "ISO-8859" ]; then
		fromcoding="$fromcoding-1"
	fi

	iconv -f $fromcoding -t $TARGET_FORMAT $file > $targetfile

	if [[ "$(file $targetfile)" != *"$TARGET_FORMAT"* ]]; then
		echo "Couldn't convert from $formcoding -> $TARGET_FORMAT"
		rm $targetfile
		exit 1
	else
		mv $targetfile $file
		echo "[  OK  ]"
	fi
done

