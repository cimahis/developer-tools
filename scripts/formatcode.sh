#!/bin/bash

## Verificacion de argumentos
if [ $# -ne 1 ]; then
	echo "Uso : formatcode Archivo.cpp"
	return 1
fi

## Verificacion de que astyle se encuentra instalado
if [ "." == ".$(which astyle)" ]; then
	echo "Por favor, instale 'astyle' primero usando el gestor de paquetes"
	return 1
fi

if ! [ -f $1 ]; then
        echo "El archivo $1 no existe, por favor indique uno valido"
	return 1
fi

# ------------------------------------------------------------
# IMPORTANTE
# Para mas informacion revise el sitio oficial de
# AStyle :
# http://astyle.sourceforge.net/astyle.html#_Indentation_Options
# ------------------------------------------------------------

astyle  --style=allman             \
        --indent=spaces=4          \
        --indent-classes           \
        --indent-switches          \
        --indent-namespaces        \
        --indent-labels            \
        --min-conditional-indent=0 \
        --break-blocks=all         \
        --pad-oper                 \
        --delete-empty-lines       \
        --align-pointer=type       \
        --break-closing-brackets   \
        --add-brackets             \
        --close-templates          \
        --remove-comment-prefix    \
        --max-code-length=80       \
        --indent-col1-comments     \
        --suffix=none              \
        --pad-header               \
        --pad-oper                 \
        $1

