#!/bin/bash

source_dir=$1
target_dir=$2

if [ -z $1 ] || [ -z $2 ]; then
    echo "Usage: $1 [SOURCE_DIR] [TARGET_DIR]"
    exit 1
fi

# checkout both directories exists
if [ -z $source_dir ]; then
    echo "$source_dir is not a valid SOURCE directory"
    exit 1
fi

if [ -z $target_dir ]; then
    echo "$target_dir is not a valid TARGET directory"
    exit 1
fi

# check doxygen is installed
if [[ `which doxygen` =~ ".*not found" ]]; then
    echo "Please install 'doxygen' package first"
    exit 1
fi

cd $target_dir
rm -rf *.html *.png *.css *.js *.db *.md5 *.svg search/

cd $source_dir
output_dir=`cat $source_dir/.doxygenrc | grep OUTPUT_DIRECTORY | sed 's/.*= //g'`
if [ -d $output_dir ]; then
    rm -rf $output_dir;
fi

doxygen .doxygenrc

mv ${output_dir}html/* $target_dir

cd $target_dir

echo "===================================================================================="
echo "  The website have been regenerated in $target_dir"
echo "  Please, now do a git add && git commit to continue"
echo "===================================================================================="
