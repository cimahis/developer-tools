@Echo off

@Rem Verificacion de ejecutable de astyle
@SET PATH_ASTYLE=
@where astyle > tmpFile
@SET /p PATH_ASTYLE= < tmpFile
@DEL tmpFile
@Rem IF "%i" == "" GOTO :ERROR_MISSING_ASTYLE ELSE SET PATH_ASTYLE=%%i
@IF "%PATH_ASTYLE%" == "" GOTO ERROR_MISSING_ASTYLE

@Rem Verificacion de archivo como argumento
@IF "%1" == "" GOTO ERROR_MISSING_FILE

@Rem Si todo esta bien se continua
@GOTO RUN_ASTYLE

:ERROR_MISSING_FILE
@Echo --------------------------------------------------------
@Echo Formateador de Codigo - Cimahis
@Echo --------------------------------------------------------
@Echo Uso:
@Echo C:\cimahis\astyle Archivo.cpp
@Echo --------------------------------------------------------
@GOTO :EOF

:ERROR_MISSING_ASTYLE
@Echo Por favor, asegurese que el directorio bin de AStyle se
@Echo encuentra en la variable PATH de Windows, e intente
@Echo nuevamente
@GOTO :EOF

@Rem ---------------------------------------------------------
@Rem IMPORTANTE
@Rem
@Rem Para mas informacion revise el sitio oficial de
@Rem AStyle :
@Rem http://astyle.sourceforge.net/astyle.html#_Indentation_Options
@Rem ---------------------------------------------------------

:RUN_ASTYLE
@astyle  --style=allman            ^
        --indent=spaces=4          ^
        --indent-classes           ^
        --indent-switches          ^
        --indent-namespaces        ^
        --indent-labels            ^
        --min-conditional-indent=0 ^
        --break-blocks=all         ^
        --pad-oper                 ^
        --delete-empty-lines       ^
        --align-pointer=type       ^
        --break-closing-brackets   ^
        --add-brackets             ^
        --close-templates          ^
        --remove-comment-prefix    ^
        --max-code-length=80       ^
        --indent-col1-comments     ^
        --suffix=none              ^
        --pad-header               ^
        --pad-oper                 ^
        %1

