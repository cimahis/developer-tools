
# Cimahis development tools

This repository contains a set of development tools to maintain [Cimahis](http://bitbucket.org/oreyes1/cimahis) application quality up-to date.

Currently, this repository supports the initial `master` version.

## Tools

#### [Dockerfile](docker/cimahis)
 - There is a Dockerfile, that will help execute unit tests, and maybe be useful to use it as development environment.

#### [Qt Creator wizard template](templates/)
 - When creating plugins for Cimahis, you will may need to use a [scaffolding](http://en.wikipedia.org/wiki/Scaffold_(programming)) tool to quickly get started and build your own plugin.
 - Also we provide scripts (bash scripting for now, a Windows' one is required for help [here](https://bitbucket.org/cimahis/developer-tools/issues/1)) get those (for now is only one) wizard templates installed.

#### [hey, is that all?](http://gifsec.com/wp-content/uploads/GIF/2014/11/no-Will-Ferrell-nope-no-way-GIF.gif)
 - No, it isn't:
    - All related scripts for unit testing automation it should be kept here.
    - In general, all utility scripts and configuration should be kept here, for later using in a Continuous Integration service.

## Documentation
 - Do you want to develop Cimahis? please see: [README](https://bitbucket.org/oreyes1/cimahis/src/master/README.md) or [HACKING](https://bitbucket.org/oreyes1/cimahis/src/master/HACKING.md) or [COLABORATE](https://bitbucket.org/oreyes1/cimahis/src/master/COLABORATE.md)

 - What about developing plugins? please see: [README](https://bitbucket.org/cimahis/cimahis-plugins/src/master/README.md)

## Colaborate

 - Please feel free reporting issues or helping fixing them.
