
# Herramientas de desarrollo de Cimahis

Este repositorio contiene un conjunto de herramientas para mantener la calidad de la aplicación [Cimahis](http://bitbucket.org/oreyes1/cimahis) al día

Actualmente, este repositorio soporta la versión `master`.

## Herramientas

#### [Dockerfile](docker/cimahis)
 - Se encontrará con un Dockerfile, que ayudará a ejecutar las pruebas unitarias y validaciones de codigo, entre otros, y posiblemente sea útil para usarlo como ambiente de desarrollo.

#### [Plantilla de Asistente de Qt Creator](templates/)
 - Cuando se crean plugins para Cimahis, se necesitará usar una herramienta de [scaffolding](http://en.wikipedia.org/wiki/Scaffold_(programming)) para rápidamente iniciar a desarrollar y construir un nuevo plugin

 - Tambien brindamos un script (por ahora en bash scripting, se necesita ayuda [acá](https://bitbucket.org/cimahis/developer-tools/issues/1) para uno en Windows) para poder instalar fácilmente (por ahora solo hay una plantilla) las plantillas de asistente para la creacion de proyectos instalada y que puedan ser usadas.

#### [...y, eso es todo?](http://gifsec.com/wp-content/uploads/GIF/2014/11/no-Will-Ferrell-nope-no-way-GIF.gif)
 - No:
    - Todos los scripts relacionados a la automatización de las pruebas unitarias.
    - En general, todos los scripts utilitarios y configuraciones deben estar en este repositorio, para su uso posterior con un servicio de Integracion Continua.

## Documentación
 - Quieres desarrollar en Cimahis? visita: [README](https://bitbucket.org/oreyes1/cimahis/src/master/README_es.md) o [HACKING](https://bitbucket.org/oreyes1/cimahis/src/master/HACKING_es.md) o [COLABORATE](https://bitbucket.org/oreyes1/cimahis/src/master/COLABORATE_es.md)

 - Que tal desarrollar plugins? visita: [README](https://bitbucket.org/cimahis/cimahis-plugins/src/master/README_es.md)

## Colaborar

 - Libremente puedes reportar incidencias o ayudar a resolverlos
