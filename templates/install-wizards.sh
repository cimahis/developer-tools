#!/bin/bash

DEFAULT_TEMPLATE_PATH="/opt/qtcreator-3.0.1/share/qtcreator/templates/wizards"

if [ "$(id -un)" != "root" ]; then
    echo
    echo "IMPORTANT: You MUST be root to install wizard templates"
    echo
    echo "+-------------------------------------------"
    echo "  Usage: sudo $0"
    echo "+-------------------------------------------"
    echo
    exit 1
fi

if [ ! -d $DEFAULT_TEMPLATE_PATH ]; then
    echo "Default QtCreator directory for wizard templates doesn't exists!"
    echo "Have you installed QtCreator in /opt ?"
    echo "Please check and try again"
    exit 1
fi

for wizfolder in `ls -d */ | tr "\\/" " "`; do
    if [ ! -f $wizfolder/wizard.xml ]; then
        echo -e "$wizfolder is missing 'wizard.xml' file"
        continue;
    fi
    echo -n "Installing $wizfolder..."
    ln -snf `pwd`/$wizfolder ${DEFAULT_TEMPLATE_PATH}/$wizfolder
    if [ -L ${DEFAULT_TEMPLATE_PATH}/$wizfolder ]; then
        echo -e "OK"
        continue
    fi
    echo -e "ERROR"
    exit 1
done
