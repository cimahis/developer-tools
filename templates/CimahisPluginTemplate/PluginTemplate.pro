# Replace "lib" with "app" to run your plugin as an application
@if "%CMH_DEBUG_WITH_MAIN%" == "true"
TEMPLATE     = app
@else
TEMPLATE     = lib
@endif
CONFIG      += plugin
@if "%CMH_CIMAHIS_DIR%" != ""
DESTDIR      = %CMH_CIMAHIS_DIR%/plugins/%ProjectName%/
@else
DESTDIR      = .
@endif

TARGET       = %ProjectName%
INCLUDEPATH  = .
QMAKE_CXXFLAGS += -std=c++11

# Include main.cpp in the next line to run your plugin as an application
@if "%CMH_DEBUG_WITH_MAIN%" == "true"
SOURCES      = main.cpp \
@else
SOURCES      = \
@endif
               %CMH_CLASS_NAME%.cpp
HEADERS      = %CMH_CLASS_NAME%.h

OTHER_FILES +=  %ProjectName%.json

# install
target.path = ../
sources.files = $$SOURCES $$HEADERS $$RESOURCES $$FORMS %ProjectName%.pro
sources.path = ../

@if "%CMH_CIMAHIS_DIR%" != ""
unix {
    QMAKE_POST_LINK += $$quote(cp $$PWD/%ProjectName%.json $$DESTDIR)
}
win32 {
    QMAKE_POST_LINK += $$quote(copy /y $$PWD/%ProjectName%.json $$DESTDIR)
}
@endif


