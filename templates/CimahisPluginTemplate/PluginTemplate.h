#ifndef %CMH_CLASS_NAME:u%_H
#define %CMH_CLASS_NAME:u%_H

#include <QObject>
#include <QtGui>

@if "%CMH_CI%" == "true"
#include "CInterface.h"
@endif
@if "%CMH_CSBI%" == "true"
#include "CStatusBarInterface.h"
@endif
@if "%CMH_CLI%" == "true"
#include "CLogInterface.h"
@endif
@if "%CMH_CII%" == "true"
#include "CImageInterface.h"
@endif
class %CMH_CLASS_NAME% : public QObject
@if "%CMH_CI%" == "true"
                         ,CInterface
@endif
@if "%CMH_CSBI%" == "true"
                         ,CStatusBarInterface
@endif
@if "%CMH_CLI%" == "true"
                         ,CLogInterface
@endif
@if "%CMH_CII%" == "true"
                         ,CImageInterface
@endif
{
    Q_OBJECT
@if "%CMH_CI%" == "true"
    Q_INTERFACES(CInterface)
@endif
@if "%CMH_CSBI%" == "true"
    Q_INTERFACES(CStatusBarInterface)
@endif
@if "%CMH_CLI%" == "true"
    Q_INTERFACES(CLogInterface)
@endif
@if "%CMH_CII%" == "true"
    Q_INTERFACES(CImageInterface)
@endif
public:
    %CMH_CLASS_NAME%();

public slots:
    void menuAction(QImage img, QString str);

@if "%CMH_CSBI%" == "true" || "%CMH_CLI%" == "true" || "%CMH_CII%" == "true"
signals:
@endif
@if "%CMH_CSBI%" == "true"
    void setStatus(QString str, int timeout);
@endif
@if "%CMH_CLI%" == "true"
    void log(QString str);
@endif
@if "%CMH_CII%" == "true"
    void mostrarImagen(QImage currentImage);
@endif
};

#endif // %CMH_CLASS_NAME:u%_H
