#include "%CMH_CLASS_NAME%.h"

%CMH_CLASS_NAME%::%CMH_CLASS_NAME%()
{
    // Constructor logic here
}

void %CMH_CLASS_NAME%::menuAction(QImage img, QString str)
{
    // remove these Q_USED
    Q_UNUSED(img)
    Q_UNUSED(str)

    // Put your logic here
}
Q_EXPORT_PLUGIN2(%ProjectName%, %CMH_CLASS_NAME%)
