#ifndef CINTERFACE_H
#define CINTERFACE_H

class CInterface
{
public:
    virtual ~CInterface(){}
};

Q_DECLARE_INTERFACE(CInterface, "org.cimahis.CInterface/1.0.0")

#endif // CINTERFACE_H
